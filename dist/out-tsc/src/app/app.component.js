import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
var AppComponent = /** @class */ (function () {
    function AppComponent(platform, splashScreen, statusBar, router, storage) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.router = router;
        this.storage = storage;
        this.initializeApp();
    }
    AppComponent.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            var wasWelcomed;
            _this.storage.get('wasWelcomedOnce').then(function (event) {
                if (event === null) {
                    wasWelcomed = false;
                    _this.storage.set('wasWelcomedOnce', false);
                    _this.router.navigateByUrl('/welcome');
                }
                else if (!event) {
                }
                else {
                    wasWelcomed = true;
                    _this.router.navigateByUrl('/home');
                }
            });
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: 'app.component.html'
        }),
        tslib_1.__metadata("design:paramtypes", [Platform,
            SplashScreen,
            StatusBar,
            Router,
            Storage])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map