import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { getColor } from '../../utils/colors';
import { DataService } from '../../services/data.service';
import { EventPage } from '../event/event.page';
var CalendarPage = /** @class */ (function () {
    function CalendarPage(
    // public navCtrl: NavController,
    // private modalCtrl: ModalController,
    storage, data, popoverCtrl) {
        this.storage = storage;
        this.data = data;
        this.popoverCtrl = popoverCtrl;
        this.selectedDay = new Date();
        this.eventSource = [];
        this.currentMonthEvents = [];
    }
    CalendarPage.prototype.ngOnInit = function () {
        this.currentDay = moment(this.data.date.currentDay).date();
        console.log('loaded');
    };
    CalendarPage.prototype.getStyleColor = function (day) {
        return getColor(day, this.data.date);
    };
    // getEvent() {
    //     this.storage.get('event').then((event) => {
    //       if (event) {
    //         this.eventSource = event;
    //         this.formatEventSource();
    //       }
    //     });
    //   }
    CalendarPage.prototype.formatEventSource = function () {
        this.eventSource.forEach(function (event) {
            event.startTime = new Date(event.startTime);
            event.endTime = new Date(event.endTime);
        });
    };
    CalendarPage.prototype.checkEvent = function (day) {
        var _this = this;
        var hasEvent = false;
        var date1 = new Date(this.data.date.year, moment(this.data.date.month, 'MMMM').month(), day);
        // const date2 = new Date(this.data.date.year, moment(this.data.date.month, 'MMMM').month(), day, 23, 59, 59);
        if (this.data.event.currentEvent === null) {
            this.data.event.currentEvent = [];
        }
        this.data.event.currentEvent.forEach(function (event) {
            var date2 = moment(date1).dayOfYear();
            var startTime = moment(event.startTime).dayOfYear();
            var endTime = moment(event.endTime).dayOfYear();
            if ((moment(event.startTime).year()) === _this.data.date.year) {
                var test = endTime - startTime;
                if (startTime <= date2 && date2 <= endTime) {
                    hasEvent = true;
                }
            }
        });
        return hasEvent;
    };
    CalendarPage.prototype.checkPayday = function (day) {
        var hasPayday = false;
        var datePayday = new Date(this.data.date.year, moment(this.data.date.month, 'MMMM').month(), day);
        this.data.payday.currentPayday.forEach(function (event) {
            if (event === moment(datePayday).format('MMM DD YYYY') && moment(datePayday).weekday() === 5) {
                hasPayday = true;
            }
        });
        return hasPayday;
    };
    CalendarPage.prototype.checkHoliday = function (day) {
        var hasHoliday = false;
        var thisHoliday = new Date(this.data.date.year, moment(this.data.date.month, 'MMMM').month(), day);
        this.data.holiday.forEach(function (event) {
            if (thisHoliday.toString() === event.date.toString()) {
                hasHoliday = true;
            }
        });
        return hasHoliday;
    };
    CalendarPage.prototype.goToLastMonth = function () {
        var t = new Date(this.data.date.year, moment(this.data.date.month, 'MMMM').month(), 0);
        this.data.changeMonth(t);
    };
    CalendarPage.prototype.goToNextMonth = function () {
        var t = new Date(this.data.date.year, moment(this.data.date.month, 'MMMM').month() + 2, 0);
        this.data.changeMonth(t);
    };
    CalendarPage.prototype.selectDate = function (day) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var clickEvent, popover;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        clickEvent = this.data.event.currentEvent;
                        return [4 /*yield*/, this.popoverCtrl.create({
                                component: EventPage,
                                componentProps: {
                                    currentEvent: clickEvent,
                                    selectedDay: this.data.date.currentDay,
                                    aDay: day,
                                    popoverController: true
                                },
                                event: day,
                                animated: true,
                                cssClass: 'my-custom-popover-css'
                            })];
                    case 1:
                        popover = _a.sent();
                        return [4 /*yield*/, popover.present()];
                    case 2:
                        _a.sent();
                        popover.onDidDismiss().then(function (data) {
                            if (data.data) {
                                var eventData = data.data;
                                eventData.startTime = new Date(data.data.startTime);
                                eventData.endTime = new Date(data.data.endTime);
                                _this.data.editEvent(data.data, clickEvent);
                            }
                            else {
                                _this.data.setEventStorage();
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    // onButtonClick(s) {
    //   if (this.segmentButton !== s) {
    //     this.segmentButton = s;
    //     this.buttonClicked = true;
    //   } else {
    //     if (this.buttonClicked) {
    //       this.buttonClicked = false;
    //     } else {
    //       this.buttonClicked = true;
    //     }
    //   }
    // }
    CalendarPage.prototype.removeItem = function (item, event) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var openAmount, i;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, event.getOpenAmount()];
                    case 1:
                        openAmount = _a.sent();
                        if (openAmount >= 140) {
                            for (i = 0; i < this.eventSource.length; i++) {
                                if (this.eventSource[i] === item) {
                                    this.eventSource.splice(i, 1);
                                    this.storage.set('event', this.eventSource);
                                }
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    CalendarPage = tslib_1.__decorate([
        Component({
            selector: 'app-calendar',
            templateUrl: './calendar.page.html',
            styleUrls: ['./calendar.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Storage,
            DataService,
            PopoverController])
    ], CalendarPage);
    return CalendarPage;
}());
export { CalendarPage };
//# sourceMappingURL=calendar.page.js.map