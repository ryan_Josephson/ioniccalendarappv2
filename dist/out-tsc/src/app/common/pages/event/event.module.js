import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { EventPage } from './event.page';
import { PipesModule } from '../../pipe/pipes.module';
import { FormComponent } from './form/form.component';
var routes = [
    {
        path: '',
        component: EventPage
    }
];
var EventPageModule = /** @class */ (function () {
    function EventPageModule() {
    }
    EventPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                PipesModule,
                RouterModule.forChild(routes)
            ],
            declarations: [EventPage, FormComponent]
        })
    ], EventPageModule);
    return EventPageModule;
}());
export { EventPageModule };
//# sourceMappingURL=event.module.js.map