import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from '@ionic/angular';
import * as moment from 'moment';
import { DataService } from '../../services/data.service';
var EventPage = /** @class */ (function () {
    function EventPage(navCtrl, navParams, popoverCtrl, data) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.data = data;
        this.selectEvent = { title: '', startTime: new Date(), endTime: new Date() };
        this.formView = false;
        this.eventSelect = false;
        this.dayEvent = [];
        this.event = { title: '', startTime: new Date(), endTime: new Date() };
        this.minDate = new Date().getFullYear();
        // console.log(preselectedDate);
        // this.event.title = '';
        // this.event.startTime = this.preSelectedDate;
        // this.event.endTime = this.preSelectedDate;
    }
    EventPage.prototype.ngOnInit = function () {
        this.preSelectedDate = this.navParams.get('selectedDay');
        // this.preSelectedDate = new Date(
        //   this.data.date.year,
        //   moment(this.data.date.month, 'MMMM').month(),
        //   this.navParams.get('aDay')
        // );
        // Home Component
        if (this.navParams.get('modalController')) {
            this.setEvent();
            this.selectEvent = this.event;
            this.formView = true;
            // Calendar Component
        }
        else if (this.navParams.get('popoverController')) {
            this.preSelectedDate = new Date(this.data.date.year, moment(this.data.date.month, 'MMMM').month(), this.navParams.get('aDay'));
            this.getDayData();
            // segment Component
        }
        else {
            this.eventSelect = true;
            // console.log(this.navParams.get('event'));
            this.selectEvent = this.navParams.get('event');
            this.dayEvent.push(this.selectEvent);
        }
    };
    EventPage.prototype.setEvent = function () {
        this.event.title = '';
        this.event.startTime = this.preSelectedDate;
        this.event.endTime = this.preSelectedDate;
    };
    EventPage.prototype.getEvent = function () {
        return this.event;
    };
    EventPage.prototype.getDayData = function () {
        var _this = this;
        this.dayEvent = [];
        this.data.event.currentEvent.forEach(function (event) {
            var startTime = moment(event.startTime).dayOfYear();
            var endTime = moment(event.endTime).dayOfYear();
            var startDate = moment(_this.preSelectedDate).dayOfYear();
            if (startTime <= startDate && startDate <= endTime) {
                _this.dayEvent.push(event);
            }
        });
    };
    EventPage.prototype.isThereAnEvent = function () {
        if (this.dayEvent.length === 0) {
            return false;
        }
        else {
            return true;
        }
    };
    EventPage.prototype.editEvent = function () {
        if (this.navParams.get('popoverController')) {
            this.formView = true;
        }
        else {
            this.selectEvent = this.dayEvent[0];
            this.formView = true;
        }
    };
    EventPage.prototype.selectChange = function () {
        this.event = this.selectEvent;
        this.eventSelect = true;
    };
    EventPage.prototype.cancel = function () {
        this.popoverCtrl.dismiss();
    };
    EventPage = tslib_1.__decorate([
        Component({
            selector: 'app-event',
            templateUrl: './event.page.html',
            styleUrls: ['./event.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            NavParams,
            PopoverController,
            DataService])
    ], EventPage);
    return EventPage;
}());
export { EventPage };
//# sourceMappingURL=event.page.js.map