import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from '@ionic/angular';
import * as moment from 'moment';
import { EventPage } from '../event.page';
var FormComponent = /** @class */ (function () {
    function FormComponent(navCtrl, navParams, popoverCtrl, selectEvent) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.popoverCtrl = popoverCtrl;
        this.selectEvent = selectEvent;
        this.event = { title: '', startTime: new Date(), endTime: new Date() };
        this.minDate = new Date().getFullYear();
        this.formButton = 'Add';
        var preselectedDate = this.navParams.get('selectedDay');
        if (!this.navParams.get('modalController')) {
            this.formButton = 'Edit';
        }
        // console.log(preselectedDate);
        this.event = this.selectEvent.getEvent();
    }
    FormComponent.prototype.save = function () {
        // this.viewCtrl.dismiss(this.event);
        this.popoverCtrl.dismiss(this.event);
    };
    FormComponent.prototype.cancel = function () {
        // this.viewCtrl.dismiss();
        this.popoverCtrl.dismiss();
    };
    FormComponent.prototype.onChange = function (events) {
        console.log(moment(events).dayOfYear());
        if (moment(events) > moment(this.event.endTime)) {
            // console.log('true');
            this.event.endTime = events;
        }
        return events;
    };
    FormComponent.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddEventPage');
    };
    FormComponent.prototype.ngOnInit = function () { };
    FormComponent = tslib_1.__decorate([
        Component({
            selector: 'app-form',
            templateUrl: './form.component.html',
            styleUrls: ['./form.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            NavParams,
            PopoverController,
            EventPage])
    ], FormComponent);
    return FormComponent;
}());
export { FormComponent };
//# sourceMappingURL=form.component.js.map