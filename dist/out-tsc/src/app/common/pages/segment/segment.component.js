import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import * as moment from 'moment';
import { DataService } from '../../services/data.service';
import { PopoverController } from '@ionic/angular';
import { EventPage } from '../event/event.page';
var SegmentComponent = /** @class */ (function () {
    function SegmentComponent(data, popoverCtrl) {
        this.data = data;
        this.popoverCtrl = popoverCtrl;
    }
    SegmentComponent.prototype.ngOnInit = function () {
    };
    SegmentComponent.prototype.removeItem = function (item, event) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var slide, i;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, event.getSlidingRatio().then(function (ratio) {
                            slide = ratio;
                            return slide;
                        })];
                    case 1:
                        _a.sent();
                        if (slide >= .5) {
                            for (i = 0; i < this.data.event.eventSource.length; i++) {
                                if (this.data.event.eventSource[i] === item) {
                                    this.data.deleteEvent(i);
                                }
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    SegmentComponent.prototype.modalEvent = function (clickEvent) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var popover;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.popoverCtrl.create({
                            component: EventPage,
                            componentProps: {
                                selectedDay: this.data.date.currentDay,
                                aDay: moment(clickEvent).date(),
                                popoverController: false,
                                event: clickEvent
                            },
                            animated: true,
                            cssClass: 'my-custom-popover-css'
                        })];
                    case 1:
                        popover = _a.sent();
                        return [4 /*yield*/, popover.present()];
                    case 2:
                        _a.sent();
                        popover.onDidDismiss().then(function (data) {
                            if (data.data) {
                                var eventData = data.data;
                                eventData.startTime = new Date(data.data.startTime);
                                eventData.endTime = new Date(data.data.endTime);
                                _this.data.editEvent(data.data, clickEvent);
                            }
                            else {
                                _this.data.setEventStorage();
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    SegmentComponent.prototype.segmentChanged = function (event) {
        this.category = event.detail.value;
    };
    SegmentComponent = tslib_1.__decorate([
        Component({
            selector: 'app-segment',
            templateUrl: './segment.component.html',
            styleUrls: ['./segment.component.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [DataService, PopoverController])
    ], SegmentComponent);
    return SegmentComponent;
}());
export { SegmentComponent };
//# sourceMappingURL=segment.component.js.map