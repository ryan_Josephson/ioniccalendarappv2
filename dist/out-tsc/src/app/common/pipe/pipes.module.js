import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { FormatToLocalPipe } from './formatToLocal/format-to-local.pipe';
import { FormatToStylePipe } from './formatToStyle/format-to-style.pipe';
import { FormatToStyle2Pipe } from './formatToStyle2/format-to-style2.pipe';
var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = tslib_1.__decorate([
        NgModule({
            declarations: [FormatToLocalPipe, FormatToStylePipe, FormatToStyle2Pipe],
            imports: [],
            exports: [FormatToLocalPipe, FormatToStylePipe, FormatToStyle2Pipe]
        })
    ], PipesModule);
    return PipesModule;
}());
export { PipesModule };
//# sourceMappingURL=pipes.module.js.map