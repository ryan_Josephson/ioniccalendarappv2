import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import * as moment from 'moment';
var FormatToLocalPipe = /** @class */ (function () {
    function FormatToLocalPipe() {
    }
    FormatToLocalPipe.prototype.transform = function (val) {
        if (val) {
            return moment(val).format();
        }
    };
    FormatToLocalPipe = tslib_1.__decorate([
        Pipe({
            name: 'formatToLocal'
        })
    ], FormatToLocalPipe);
    return FormatToLocalPipe;
}());
export { FormatToLocalPipe };
//# sourceMappingURL=format-to-local.pipe.js.map