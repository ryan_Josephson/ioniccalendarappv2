import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import * as moment from 'moment';
var FormatToStylePipe = /** @class */ (function () {
    function FormatToStylePipe() {
    }
    FormatToStylePipe.prototype.transform = function (val) {
        if (val) {
            return moment(val).format('ddd  MMM DD YYYY hh:mm A');
        }
    };
    FormatToStylePipe = tslib_1.__decorate([
        Pipe({
            name: 'formatToStyle'
        })
    ], FormatToStylePipe);
    return FormatToStylePipe;
}());
export { FormatToStylePipe };
//# sourceMappingURL=format-to-style.pipe.js.map