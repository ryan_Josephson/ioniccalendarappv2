import * as tslib_1 from "tslib";
import { Pipe } from '@angular/core';
import * as moment from 'moment';
var FormatToStyle2Pipe = /** @class */ (function () {
    function FormatToStyle2Pipe() {
    }
    FormatToStyle2Pipe.prototype.transform = function (val) {
        if (val) {
            return moment(val).format('MMM DD YYYY');
        }
    };
    FormatToStyle2Pipe = tslib_1.__decorate([
        Pipe({
            name: 'formatToStyle2'
        })
    ], FormatToStyle2Pipe);
    return FormatToStyle2Pipe;
}());
export { FormatToStyle2Pipe };
//# sourceMappingURL=format-to-style2.pipe.js.map