import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { PaydayInfo } from '../../common/models/paydays';
import { CurrentMonthInfo } from '../../common/models/currentMonthInfo';
import { Holiday } from '../../common/models/holidays';
import { EventInfo } from '../../common/models/events';
import * as moment from 'moment';
var DataService = /** @class */ (function () {
    function DataService(storage) {
        this.storage = storage;
        this.currentWelcomedOnce = true;
        this.event = new EventInfo();
        this.payday = new PaydayInfo();
        this.date = new CurrentMonthInfo();
        this.monthNames = [];
        this.actualYear = new Date().getFullYear();
        this.holiday = [
            new Holiday('New Years Day', new Date('January 1, 2019')),
            new Holiday('Martin Luther', new Date('January 21, 2019')),
            new Holiday('Presidents Day', new Date('February 18, 2019')),
            new Holiday('Memorial Day', new Date('May 27, 2019')),
            new Holiday('Independence Day', new Date('July 4, 2019')),
            new Holiday('Labor Day', new Date('September 2, 2019')),
            new Holiday('Columbus Day', new Date('October 14, 2019')),
            new Holiday('Veterans Day', new Date('November 11, 2019')),
            new Holiday('Thanksgiving Day', new Date('November 28, 2019')),
            new Holiday('Christmas Day', new Date('December 25, 2019'))
        ];
        this.monthNames = ['January', 'February', 'March', 'April', 'May',
            'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        this.setEventStorage();
        this.getDaysOfMonth();
        this.getPayday();
        this.paydayThisMonth();
        this.loadHolidayThisMonth();
        // this.getWelcomeStatus();
    }
    DataService.prototype.getWelcomeStatus = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.get('wasWelcomedOnce').then(function (event) {
                            if (event === null) {
                                _this.currentWelcomedOnce = true;
                                _this.storage.set('wasWelcomedOnce', true);
                            }
                            else {
                                _this.currentWelcomedOnce = false;
                            }
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DataService.prototype.setWelcomeStatus = function () {
        this.storage.set('wasWelcomedOnce', true);
    };
    DataService.prototype.getPaydayData = function () {
        return this.payday;
    };
    DataService.prototype.getMonthData = function (daysInThisMonth, daysInLastMonth, daysInNextMonth, lastYear) {
        daysInThisMonth = this.daysInThisMonth;
        daysInLastMonth = this.daysInLastMonth;
        daysInNextMonth = this.daysInNextMonth;
        lastYear = this.lastYear;
    };
    DataService.prototype.getPayday = function () {
        var _this = this;
        this.payday.data = [];
        var _loop_1 = function (i) {
            // if (this.payday.data === null) {
            //   this.payday.data = [''];
            //   this.payday.currentPayday = [''];
            // }
            var evenPayday = void 0;
            if (i % 2 === 0) {
                var events_1 = this_1.payday.data;
                evenPayday = moment().day(5).week(i).format('MMM DD YYYY');
                this_1.payday.data.push(evenPayday);
                setTimeout(function () {
                    _this.payday.data = events_1;
                });
            }
        };
        var this_1 = this;
        for (var i = 1; i <= moment(this.date.year).weeksInYear(); i++) {
            _loop_1(i);
        }
    };
    DataService.prototype.paydayThisMonth = function () {
        var _this = this;
        this.payday.currentPayday = [];
        this.payday.data.forEach(function (event) {
            var l = new Date(event);
            if (_this.monthNames[l.getMonth()] === _this.date.month && (l.getFullYear() === _this.date.year)) {
                _this.payday.currentPayday.push(event);
                return true;
            }
            else {
                return false;
            }
        });
    };
    DataService.prototype.getDaysOfMonth = function () {
        var currentDay = this.date.currentDay;
        var currentDate;
        this.daysInThisMonth = new Array();
        this.daysInLastMonth = new Array();
        this.daysInNextMonth = new Array();
        this.date.month = this.monthNames[currentDay.getMonth()];
        this.date.year = currentDay.getFullYear();
        this.lastYear = this.date.year - 1;
        if (currentDay.getMonth() === new Date().getMonth() && currentDay.getFullYear() === this.actualYear) {
            currentDate = new Date().getDate();
        }
        else {
            currentDate = 999;
        }
        var firstDayThisMonth = new Date(currentDay.getFullYear(), currentDay.getMonth(), 1).getDay();
        var prevNumOfDays = new Date(currentDay.getFullYear(), currentDay.getMonth(), 0).getDate();
        for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
            this.daysInLastMonth.push(i);
        }
        var thisNumOfDays = new Date(currentDay.getFullYear(), currentDay.getMonth() + 1, 0).getDate();
        for (var j = 0; j < thisNumOfDays; j++) {
            this.daysInThisMonth.push(j + 1);
        }
        var lastDayThisMonth = new Date(currentDay.getFullYear(), currentDay.getMonth() + 1, 0).getDay();
        for (var k = 0; k < (6 - lastDayThisMonth); k++) {
            this.daysInNextMonth.push(k + 1);
        }
        var totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
        if (totalDays < 36) {
            for (var l = (7 - lastDayThisMonth); l < ((7 - lastDayThisMonth) + 7); l++) {
                this.daysInNextMonth.push(l);
            }
        }
    };
    DataService.prototype.changeMonth = function (t) {
        this.date.currentDay = t;
        this.getDaysOfMonth();
        this.paydayThisMonth();
        this.loadHolidayThisMonth();
        this.loadEventThisMonth();
    };
    DataService.prototype.loadEventThisMonth = function () {
        var _this = this;
        this.event.currentEvent = [];
        if (this.event.eventSource === null) {
            this.event.eventSource = [];
        }
        this.event.eventSource.forEach(function (event) {
            var r = moment(event.startTime).format('MMMM');
            var t = moment(event.endTime).format('MMMM');
            if (r === _this.date.month && ((moment(event.startTime).year()) === _this.date.year)) {
                _this.event.currentEvent.push(event);
            }
            else if (t === _this.date.month && ((moment(event.endTime).year()) === _this.date.year)) {
                _this.event.currentEvent.push(event);
            }
            else {
            }
        });
    };
    DataService.prototype.loadHolidayThisMonth = function () {
        var _this = this;
        this.currentHoliday = [{ name: 'blah', date: new Date('January 1,2016') }];
        var testForIndex = true;
        this.holiday.forEach(function (event) {
            var t = moment(event.date).format('MMMM');
            var p = { name: event.name, date: event.date };
            // console.log(this.date.month + ' event ' + t + ' ' + this.date.year + ' ' + moment(event.date).year());
            if (t === _this.date.month && (moment(event.date).year() === _this.date.year)) {
                if (testForIndex) {
                    _this.currentHoliday.push(p);
                    _this.currentHoliday.splice(0, 1);
                    testForIndex = false;
                }
                else {
                    _this.currentHoliday.push(p);
                }
            }
        });
        if (testForIndex) {
            this.currentHoliday = null;
        }
    };
    DataService.prototype.saveEvent = function (data) {
        var _this = this;
        this.event.eventSource = [];
        setTimeout(function () {
            _this.event.eventSource = data;
            _this.storage.set('event', _this.event.eventSource);
            _this.loadEventThisMonth();
        });
    };
    DataService.prototype.deleteEvent = function (item) {
        this.event.eventSource.splice(item, 1);
        this.storage.set('event', this.event.eventSource);
        this.loadEventThisMonth();
    };
    DataService.prototype.setEventStorage = function () {
        var _this = this;
        this.event.eventSource = [];
        this.storage.get('event').then(function (event) {
            if (event) {
                _this.event.eventSource = event;
                _this.loadEventThisMonth();
            }
        });
    };
    DataService.prototype.editEvent = function (newEvent, oldEvent) {
        for (var i = 0; i < this.event.eventSource.length; i++) {
            if (this.event.eventSource[i] === oldEvent) {
                this.event.eventSource.splice(i, 1);
                this.event.eventSource.push(newEvent);
                this.storage.set('event', this.event.eventSource);
                this.loadEventThisMonth();
            }
        }
    };
    DataService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Storage])
    ], DataService);
    return DataService;
}());
export { DataService };
//# sourceMappingURL=data.service.js.map