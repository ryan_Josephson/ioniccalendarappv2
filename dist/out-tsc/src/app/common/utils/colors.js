import * as moment from 'moment';
export function getColor(day, currentMonthInfo) {
    var color = [];
    var currentWeek = new Date(currentMonthInfo.year, moment(currentMonthInfo.month, 'MMMM').month(), day);
    var curDate = currentWeek;
    if (moment(currentWeek).year() === 2019) {
        currentWeek = moment(currentWeek).week() + 4;
        if (moment(curDate).dayOfYear() > 362) {
            currentWeek = 3;
        }
    }
    else if (moment(currentWeek).year() === 2018) {
        currentWeek = moment(currentWeek).week();
        if (moment(curDate).dayOfYear() > 363) {
            currentWeek = 53;
        }
    }
    else if (moment(currentWeek).year() > 2019) {
        currentWeek = moment(currentWeek).week() + 2;
        if (moment(curDate).dayOfYear() > 361) {
            currentWeek = 55;
        }
    }
    else {
        currentWeek = moment(currentWeek).week();
    }
    currentWeek %= 6;
    switch (currentWeek) {
        case 0:
            color = ['red', 'black', '#FFEC00', 'blue', 'green', '#733100'];
            break;
        // return color; red black yellow blue green brown
        case 1:
            color = ['#733100', 'red', 'black', '#FFEC00', 'blue', 'green'];
            break;
        // return color; brown red black yellow blue green
        case 2:
            color = ['green', '#733100', 'red', 'black', '#FFEC00', 'blue'];
            // return color; green brown red black yellow blue
            break;
        case 3:
            color = ['blue', 'green', '#733100', 'red', 'black', '#FFEC00'];
            break;
        // return color; blue green brown red black yellow
        case 4:
            color = ['#FFEC00', 'blue', 'green', '#733100', 'red', 'black'];
            break;
        // return color; yellow blue green brown red black
        case 5:
            color = ['black', '#FFEC00', 'blue', 'green', '#733100', 'red'];
            break;
        // return color; black yellow blue green brown red
    }
    switch (moment(curDate).day()) {
        case 0: // sun
            return color[1];
        case 1: // mon
            return color[2];
        case 2: // tue
            return color[3];
        case 3: // wed
            return color[4];
        case 4: // thu
            return color[5];
        case 5: // fri
            return color[0];
        case 6: // sat
            return color[0];
    }
}
//# sourceMappingURL=colors.js.map