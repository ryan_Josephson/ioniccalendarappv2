var EventInfo = /** @class */ (function () {
    function EventInfo(reqData) {
        if (reqData === void 0) { reqData = null; }
        this.eventSource = reqData && reqData.eventSource || null;
        this.currentEvent = reqData && reqData.currentEvent || null;
    }
    return EventInfo;
}());
export { EventInfo };
//# sourceMappingURL=events.js.map