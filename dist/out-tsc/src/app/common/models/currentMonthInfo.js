var CurrentMonthInfo = /** @class */ (function () {
    function CurrentMonthInfo(reqData) {
        if (reqData === void 0) { reqData = null; }
        this.year = reqData && reqData.year || null;
        this.currentDay = reqData && reqData.currentDay || new Date('January 1,2021');
        this.month = reqData && reqData.month || null;
    }
    return CurrentMonthInfo;
}());
export { CurrentMonthInfo };
//# sourceMappingURL=currentMonthInfo.js.map