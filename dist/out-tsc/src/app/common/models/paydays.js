var PaydayInfo = /** @class */ (function () {
    function PaydayInfo(reqData) {
        if (reqData === void 0) { reqData = null; }
        this.data = [];
        this.currentPayday = [];
        this.data = reqData && reqData.data || null;
        this.currentPayday = reqData && reqData.currentPayday || null;
    }
    return PaydayInfo;
}());
export { PaydayInfo };
//# sourceMappingURL=paydays.js.map