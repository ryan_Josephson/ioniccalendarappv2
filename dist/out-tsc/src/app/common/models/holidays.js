var Holiday = /** @class */ (function () {
    function Holiday(name, date) {
        this.name = name;
        this.date = date;
    }
    return Holiday;
}());
export { Holiday };
//# sourceMappingURL=holidays.js.map