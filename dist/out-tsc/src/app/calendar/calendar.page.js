import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, ModalController, PopoverController, AlertController } from '@ionic/angular';
import { Holiday } from '../common/models/holidays';
import { Storage } from '@ionic/storage';
var CalendarPage = /** @class */ (function () {
    function CalendarPage(navCtrl, modalCtrl, storage, popoverController, alertCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.popoverController = popoverController;
        this.alertCtrl = alertCtrl;
        this.selectedDay = new Date();
        this.eventSource = [];
        this.currentMonthEvents = [];
        this.holiday = [
            new Holiday('New Years Day', new Date('January 1, 2019')),
            new Holiday('Martin Luther', new Date('January 21, 2019')),
            new Holiday('Presidents Day', new Date('Febuary 18, 2019')),
            new Holiday('Memorial Day', new Date('May 27, 2019')),
            new Holiday('Independence Day', new Date('July 4, 2019')),
            new Holiday('Labor Day', new Date('September 2, 2019')),
            new Holiday('Columbus Day', new Date('October 14, 2019')),
            new Holiday('Veterans Day', new Date('November 11, 2019')),
            new Holiday('Thanksgiving Day', new Date('November 28, 2019')),
            new Holiday('Christmas Day', new Date('December 25, 2019'))
        ];
    }
    CalendarPage.prototype.ngOnInit = function () {
        console.log('loaded');
    };
    CalendarPage = tslib_1.__decorate([
        Component({
            selector: 'app-calendar',
            templateUrl: './calendar.page.html',
            styleUrls: ['./calendar.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController,
            ModalController,
            Storage,
            PopoverController,
            AlertController])
    ], CalendarPage);
    return CalendarPage;
}());
export { CalendarPage };
//# sourceMappingURL=calendar.page.js.map