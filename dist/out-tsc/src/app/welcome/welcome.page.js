import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../common/services/data.service';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
var WelcomePage = /** @class */ (function () {
    function WelcomePage(router, data, emailComposer) {
        this.router = router;
        this.data = data;
        this.emailComposer = emailComposer;
    }
    WelcomePage.prototype.ngOnInit = function () {
    };
    WelcomePage.prototype.gotoHome = function () {
        this.data.setWelcomeStatus();
        this.router.navigateByUrl('/home');
    };
    WelcomePage.prototype.contact = function () {
        console.log('hi');
        this.emailComposer.isAvailable().then(function (available) {
            if (available) {
            }
        });
        var email = {
            to: 'PostaCalendar@gmail.com',
            subject: 'Postal Calendar',
            body: 'Let me Know what you think!',
            isHtml: true
        };
        this.emailComposer.open(email);
    };
    WelcomePage = tslib_1.__decorate([
        Component({
            selector: 'app-welcome',
            templateUrl: './welcome.page.html',
            styleUrls: ['./welcome.page.scss'],
            providers: [EmailComposer]
        }),
        tslib_1.__metadata("design:paramtypes", [Router, DataService, EmailComposer])
    ], WelcomePage);
    return WelcomePage;
}());
export { WelcomePage };
//# sourceMappingURL=welcome.page.js.map