import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { DataService } from '../common/services/data.service';
import { EventPage } from '../common/pages/event/event.page';
import { Router } from '@angular/router';
var HomePage = /** @class */ (function () {
    function HomePage(data, popoverCtrl, router) {
        this.data = data;
        this.popoverCtrl = popoverCtrl;
        this.router = router;
    }
    HomePage.prototype.addEvent = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var popover;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.popoverCtrl.create({
                            component: EventPage,
                            componentProps: {
                                selectedDay: this.data.date.currentDay,
                                modalController: true
                            },
                            cssClass: 'my-custom-modal-css'
                        })];
                    case 1:
                        popover = _a.sent();
                        return [4 /*yield*/, popover.present()];
                    case 2:
                        _a.sent();
                        popover.onDidDismiss().then(function (data) {
                            // let eventData = data;
                            // this.data.saveEvent(data);
                            if (data.data) {
                                var eventData = data.data;
                                eventData.startTime = new Date(data.data.startTime);
                                // eventData.startTime = moment(new Date(eventData.startTime)).format('ddd MMM DD YYYY HH:mm:ss');
                                eventData.endTime = new Date(data.data.endTime);
                                // eventData.endTime = moment(new Date(eventData.startTime)).format('ddd MMM DD YYYY HH:mm:ss');
                                if (_this.data.event.eventSource != null) {
                                    var events = _this.data.event.eventSource;
                                    events.push(eventData);
                                    _this.data.saveEvent(events);
                                }
                                else {
                                    _this.data.saveEvent(eventData);
                                    console.log(eventData);
                                }
                            }
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.gotoWelcome = function () {
        this.router.navigateByUrl('/welcome');
    };
    HomePage = tslib_1.__decorate([
        Component({
            selector: 'app-home',
            templateUrl: 'home.page.html',
            styleUrls: ['home.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [DataService, PopoverController, Router])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.page.js.map