import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.page.html',
  styleUrls: ['./alert.page.scss'],
})
export class AlertPage implements OnInit {

  constructor(private navParams: NavParams, private popoverCtrl: PopoverController) { }
  color: any;
  ngOnInit() {
    this.color = this.navParams.get('color');
  }
  save(event) {
    this.popoverCtrl.dismiss(event);
  }

}
