import { Component, OnInit } from '@angular/core';
import { ModalController, IonItemSliding, } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { getColor } from '../../utils/colors';
import { DataService } from '../../services/data.service';
import { EventPage } from '../event/event.page';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.page.html',
  styleUrls: ['./calendar.page.scss'],
})
export class CalendarPage implements OnInit {
  selectedDay = new Date();
  eventSource = [];
  currentMonthEvents = [];
  eventList: any;
  selectedEvent: any;
  isSelected: any;
  buttonClicked: boolean;
  segmentButton: any;
  color: [];
  constructor(
    private storage: Storage,
    public data: DataService,
    public modalCtrl: ModalController
    ) {}
  swipeLeft(event) {
      this.goToNextMonth();
  }
  swipeRight(event) {
    this.goToLastMonth();
  }
  ngOnInit() {
    console.log('loaded');
  }
  getStyleColor(day: number) {
    return getColor(day, this.data.date, this.data.color, this.data.patternWeek);
  }
  formatEventSource() {
    this.eventSource.forEach(event => {
      event.startTime = new Date(event.startTime);
      event.endTime = new Date(event.endTime);
    });
  }

  checkEvent(day: number) {
    let hasEvent = false;
    const date1 = new Date(this.data.date.year, moment(this.data.date.month, 'MMMM').month(), day);
    if (this.data.event.currentEvent === null) {
      this.data.event.currentEvent = [];
    }
    this.data.event.currentEvent.forEach(event => {
      const date2 = moment(date1).dayOfYear();
      const startTime = moment(event.startTime).dayOfYear();
      const endTime = moment(event.endTime).dayOfYear();
      if ((moment(event.startTime).year()) === this.data.date.year) {
        const test = endTime - startTime;
        if (startTime <= date2 && date2 <= endTime) {
            hasEvent = true;
        }
      }
    });
    return hasEvent;
  }
   checkPayday(day) {
    let hasPayday = false;
    const datePayday = new Date(this.data.date.year, moment(this.data.date.month, 'MMMM').month(), day);
    this.data.payday.currentPayday.forEach(event => {
      if (event === moment(datePayday).format('MMM DD, YYYY') && moment(datePayday).weekday() === 5) {
        hasPayday = true;
      }
    });

    return hasPayday;
  }
    checkHoliday(day) {
    let hasHoliday = false;
    const thisHoliday = new Date(this.data.date.year, moment(this.data.date.month, 'MMMM').month(), day);
    this.data.holiday.forEach(event => {
      if (thisHoliday.toString() === event.date.toString()) {
        hasHoliday = true;
      }
    });

    return hasHoliday;
  }
  goToLastMonth() {
    const t = new Date(this.data.date.year, moment(this.data.date.month, 'MMMM').month() - 1, 1);
    this.data.changeMonth(t);

  }

  goToNextMonth() {
    const t = new Date(this.data.date.year, moment(this.data.date.month, 'MMMM').month() + 1, 1);
    this.data.changeMonth(t);
  }
  async selectDate(day) {
    const clickEvent = this.data.event.currentEvent;
    const popover = await this.modalCtrl.create({
      component: EventPage,
      componentProps: {
        currentEvent: clickEvent,
        selectedDay: this.data.date.currentDay,
        aDay: day,
        popoverController: true
      },
      // event: day,
      animated: true,
      cssClass: 'my-custom-modalCtrl-css'
    });
    await popover.present();
    popover.onDidDismiss().then((data: any) => {
      if (data.data) {
        const eventData: any = data.data.event;
        eventData.startTime = new Date(data.data.event.startTime);
        eventData.endTime = new Date(data.data.event.endTime);
        this.data.editEvent(data.data.event, clickEvent, data.data.newEvent);
      } else {
        this.data.setEventStorage();
      }
    });
  }
  async removeItem(item, event: IonItemSliding) {
    const openAmount = await event.getOpenAmount();
    if (openAmount >= 140) {
      for (let i = 0; i < this.eventSource.length; i++) {
        if (this.eventSource[i] === item) {
          this.eventSource.splice(i, 1);
          this.storage.set('event', this.eventSource);
        }
      }
    }
  }
}
