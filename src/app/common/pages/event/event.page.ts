import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from '@ionic/angular';
import * as moment from 'moment';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.page.html',
  styleUrls: ['./event.page.scss']
})
export class EventPage implements OnInit {
  selectEvent = {title: '', startTime: new Date(), endTime: new Date()};
  formView = false;
  eventSelect = false;
  preSelectedDate: any;
  dayEvent: any = [];
  event = { title: '', startTime: new Date(), endTime: new Date()};
  minDate = new Date().getFullYear();
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private data: DataService,
    private modalCtrl: ModalController
  ) {
    // console.log(preselectedDate);
    // this.event.title = '';
    // this.event.startTime = this.preSelectedDate;
    // this.event.endTime = this.preSelectedDate;
  }

  ngOnInit() {
    this.preSelectedDate = this.navParams.get('selectedDay');
    // this.preSelectedDate = new Date(
    //   this.data.date.year,
    //   moment(this.data.date.month, 'MMMM').month(),
    //   this.navParams.get('aDay')
    // );

    // Home Component
    if (this.navParams.get('modalController')) {
      this.setEvent();
      this.selectEvent = this.event;
      this.formView = true;
      // Calendar Component
    } else if (this.navParams.get('popoverController')) {
      this.preSelectedDate = new Date(this.data.date.year, moment(this.data.date.month, 'MMMM').month(), this.navParams.get('aDay'));
      this.setEvent();
      this.getDayData();
      // segment Component
    } else {
      this.eventSelect = true;
      // console.log(this.navParams.get('event'));
      this.selectEvent = this.navParams.get('event');
      this.dayEvent.push(this.selectEvent);
    }
  }
  setEvent() {
    this.event.title = '';
    this.event.startTime = this.preSelectedDate;
    this.event.endTime = this.preSelectedDate;
  }
  getEvent() {
    return this.event;
  }
  getDayData() {
    this.dayEvent = [];
    this.data.event.currentEvent.forEach(event => {
      const startTime = moment(event.startTime).dayOfYear();
      const endTime = moment(event.endTime).dayOfYear();
      const startDate = moment(this.preSelectedDate).dayOfYear();
      if ( startTime <= startDate && startDate <= endTime
      ) {
        this.dayEvent.push(event);
      }
    });
  }
  isThereAnEvent() {
    if (this.dayEvent.length === 0 || this.dayEvent === []) {
      return false;
    } else {
      return true;
    }
  }
  editEvent() {
    if (this.navParams.get('popoverController')) {
      this.formView = true;
    } else {
      this.selectEvent = this.dayEvent[0];
      this.formView = true;
    }
  }
  addEvent() {
    this.formView = true;
  }
  selectChange() {
    this.event = this.selectEvent;
    this.eventSelect = true;
  }
  cancel() {
    this.modalCtrl.dismiss();
  }
}
