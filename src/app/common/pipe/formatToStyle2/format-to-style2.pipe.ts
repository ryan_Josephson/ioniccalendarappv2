import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatToStyle2'
})
export class FormatToStyle2Pipe implements PipeTransform {

  transform(val) {
    if (val) {
      return moment(val).format('MMM DD, YYYY');
    }
  }

}
