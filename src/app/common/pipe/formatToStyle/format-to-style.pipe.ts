import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatToStyle'
})
export class FormatToStylePipe implements PipeTransform {

  transform(val) {
    if (val) {
      return moment(val).format('ddd  MMM DD, YYYY hh:mm A');
    }
  }

}
