import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormatToLocalPipe } from './formatToLocal/format-to-local.pipe';
import { FormatToStylePipe } from './formatToStyle/format-to-style.pipe';
import { FormatToStyle2Pipe } from './formatToStyle2/format-to-style2.pipe';
@NgModule({
  declarations: [FormatToLocalPipe, FormatToStylePipe, FormatToStyle2Pipe],
  imports: [],
  exports: [FormatToLocalPipe, FormatToStylePipe, FormatToStyle2Pipe]
})
export class PipesModule {}
