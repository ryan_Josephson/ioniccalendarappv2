import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { PaydayInfo } from '../../common/models/paydays';
import { CurrentMonthInfo } from '../../common/models/currentMonthInfo';
import { EventInfo } from '../../common/models/events';
import { setHol } from '../utils/holidays';
import * as moment from 'moment';
import { getPattern } from '../utils/colorPattern';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  currentWelcomedOnce = true;
  event: EventInfo = new EventInfo();
  payday: PaydayInfo = new PaydayInfo();
  date: CurrentMonthInfo = new CurrentMonthInfo();
  monthNames = [];
  currentDay = new Date();
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  lastYear: number;
  nextYear: number;
  color: any[] = ['blue', 'green', '#733100', 'red', 'black', '#FFEC00'];
  selectEvent: string;
  actualYear: number = new Date().getFullYear();
  holiday = [];
  currentHoliday: [{name: string, date: Date} ];
  pattern: any;
  patternWeek: number;

  constructor(public storage: Storage) {
    this.monthNames = ['January', 'February', 'March', 'April', 'May',
    'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    this.setEventStorage();
    this.getDaysOfMonth();
    this.getPayday();
    this.paydayThisMonth();
    this.getHoliday();
    this.loadHolidayThisMonth();
    this.getDistrictColor();
    // this.pattern = getPattern(this.date.year);
    this.pattern = getPattern();
    const found = this.pattern.find(e => e.year === this.date.year);
    this.patternWeek = found.p;
    // this.getWelcomeStatus();
    // this.getDistrictColor();
  }
  async getWelcomeStatus() {
    await this.storage.get('wasWelcomeOnce').then((event) => {
      if (event === null) {
        this.currentWelcomedOnce = true;
        this.storage.set('wasWelcomeOnce', true);
      } else {
        this.currentWelcomedOnce = false;
      }
    });
  }
  // getDistrictColor() {
  //   return this.color;
  // }
  getDistrictColor() {
    this.storage.get('theme').then((event) => {
      if (event === null) {
        this.color = ['blue', 'green', '#733100', 'red', 'black', '#FFEC00'];
        this.selectEvent = '385';
      } else {
        this.color = event.color;
        this.selectEvent = event.selectEvent;
      }
    });
  }
  setDistrictColor(district: any[]) {
    this.color = district;
  }
  setWelcomeStatus() {
    this.storage.set('wasWelcomeOnce', true);
  }
  getPaydayData() {
    return this.payday;
  }
  getHoliday() {
    this.holiday = setHol(this.date);
  }
  getPayday() {
    this.payday.data = [];
    for (let i = 1; i <= moment(this.date.year).weeksInYear(); i++) {
        let evenPayday;
        if ( i % 2 === 0) {
          const events = this.payday.data;
          evenPayday = moment().year(this.date.year).day(5).week(i).format('MMM DD, YYYY');
          this.payday.data.push(evenPayday);
          setTimeout(() => {
            this.payday.data = events;
          });
        }
    }
  }
  paydayThisMonth() {
    this.payday.currentPayday = [];
    this.payday.data.forEach(event => {
      const l = new Date(event);
      if (this.monthNames[l.getMonth()] === this.date.month && (l.getFullYear() === this.date.year)) {
        this.payday.currentPayday.push(event);
        return true;
      } else {
      return false;
      }
    });
  }
  getDaysOfMonth() {
    const currentDay = this.date.currentDay;
    let currentDate;
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.date.month = this.monthNames[currentDay.getMonth()];
    this.date.year = currentDay.getFullYear();
    if(this.date.year === this.lastYear || this.date.year === this.nextYear) {
      this.getHoliday();
      this.getPayday();
      const found = this.pattern.find(e => e.year === this.date.year);
      this.patternWeek = found.p;
    }
    this.nextYear = this.date.year + 1;
    this.lastYear = this.date.year - 1;
    if (currentDay.getMonth() === new Date().getMonth() && currentDay.getFullYear() === this.actualYear) {
      currentDate = new Date().getDate();
    } else {
      currentDate = 999;
    }

    const firstDayThisMonth = new Date(currentDay.getFullYear(), currentDay.getMonth(), 1).getDay();
    const prevNumOfDays = new Date(currentDay.getFullYear(), currentDay.getMonth(), 0).getDate();
    for (let i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
      this.daysInLastMonth.push(i);
    }

    const thisNumOfDays = new Date(currentDay.getFullYear(), currentDay.getMonth() + 1, 0).getDate();
    for (let j = 0; j < thisNumOfDays; j++) {
      this.daysInThisMonth.push(j + 1);
    }

    const lastDayThisMonth = new Date(currentDay.getFullYear(), currentDay.getMonth() + 1, 0).getDay();
    for (let k = 0; k < (6 - lastDayThisMonth); k++) {
      this.daysInNextMonth.push(k + 1);
    }
    const totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
    if (totalDays < 36) {
      for (let l = (7 - lastDayThisMonth); l < ((7 - lastDayThisMonth) + 7); l++) {
        this.daysInNextMonth.push(l);
      }
    }
  }
  changeMonth(t) {
    this.date.currentDay = t;
    this.getDaysOfMonth();
    this.paydayThisMonth();
    this.loadHolidayThisMonth();
    this.loadEventThisMonth();
  }
  loadEventThisMonth() {
    this.event.currentEvent = [];
    if (this.event.eventSource === null) {
      this.event.eventSource = [];
    }
    this.event.eventSource.forEach(event => {
      const r = moment(event.startTime).format('MMMM');
      const t = moment(event.endTime).format('MMMM');

      if (r === this.date.month && ((moment(event.startTime).year()) === this.date.year)) {
        this.event.currentEvent.push(event);
      } else if (t === this.date.month && ((moment(event.endTime).year()) === this.date.year)) {
        this.event.currentEvent.push(event);
      } else {
      }
    });


  }
  loadHolidayThisMonth() {
      this.currentHoliday = [{name: 'blah', date: new Date('January 1,2016')}];
      let testForIndex = true;


      this.holiday.forEach(event => {
      const t = moment(event.date).format('MMMM');
      const p = {name: event.name, date: event.date};
      // console.log(this.date.month + ' event ' + t + ' ' + this.date.year + ' ' + moment(event.date).year());
      if (t === this.date.month && (moment(event.date).year() === this.date.year)) {
        if (testForIndex) {
          this.currentHoliday.push(p);
          this.currentHoliday.splice(0, 1);
          testForIndex = false;
        } else {
          this.currentHoliday.push(p);
        }
       }
    });
      if (testForIndex) {
        this.currentHoliday = null;
      }
  }
  saveEvent(data) {
    this.event.eventSource = [];
    setTimeout(() => {
      this.event.eventSource = data;
      this.storage.set('event', this.event.eventSource);
      this.loadEventThisMonth();
    });

  }
  deleteEvent(item: number) {
    this.event.eventSource.splice(item, 1);
    this.storage.set('event', this.event.eventSource);
    this.loadEventThisMonth();
  }
  setEventStorage() {
    this.event.eventSource = [];
    this.storage.get('event').then((event) => {
      if (event) {
        this.event.eventSource = event;
        this.loadEventThisMonth();
      }
    });
  }
  editEvent(newEvent: [], oldEvent, testForNewEvent) {
    if (!testForNewEvent) {
      this.event.eventSource.push(newEvent);
      this.storage.set('event', this.event.eventSource);
      this.loadEventThisMonth();
    } else {
      for (let i = 0; i < this.event.eventSource.length; i++) {
        if (this.event.eventSource[i] === oldEvent) {
          this.event.eventSource.splice(i, 1);
          this.event.eventSource.push(newEvent);
          this.storage.set('event', this.event.eventSource);
          this.loadEventThisMonth();
        }
      }
    }
  }
}
