import * as moment from 'moment';
import { CurrentMonthInfo } from '../models/currentMonthInfo';
import { Holiday } from '../models/holidays';
let holiday = [];
function getHoliday(year:number) {
    for(let i = 0; i < 12; i++) {
        switch (i) {
            case 0: //January
                holiday.push(new Holiday('New Years Day', new Date(year, i, 1)))
                setHoliday(year, i, 3, 1, 'Martin Luther');
            break;
            case 1: //February
                setHoliday(year, i, 3, 1,"President's Day");
            break;
            case 4: //May
                setHoliday(year, i, 0, 1,"Memorial Day");
            break;
            case 6: //July
                holiday.push(new Holiday("Independence Day", new Date(year, i, 4)));
            break;
            case 8: //September
                setHoliday(year, i, 1, 1,"Labor Day");
            break;
            case 9: //October
                setHoliday(year, i, 2, 1,"Columbus Day");
                holiday.push(new Holiday("Halloween", new Date(year, i, 31)));
            break;
            case 10: //November
                holiday.push(new Holiday("Veterans Day", new Date(year,i, 11)));
                setHoliday(year, i, 4, 4,"Thanksgiving Day");
            break;
            case 11: //December
                holiday.push(new Holiday("Christmas", new Date(year,i, 25)));
            break;

        }

    }
}
function setHoliday(year, month, whatWeek, dayOfWeek, holidayName) {
    let weekCounter = 0;
    let hol;
    let daysInMonth = moment().year(year).month(month).daysInMonth();
    for (let i = 1; i <= daysInMonth; i++) {
      if(moment().year(year).month(month).date(i).day() === dayOfWeek) {
        if(whatWeek !== 0) {
          weekCounter++;
          if(weekCounter === whatWeek) {
            // holiday.push(new Holiday(holidayName, new Date(year, month, i)));
            hol = new Holiday(holidayName, new Date(year, month, i));
          }
        } else {
            // holiday.push(new Holiday(holidayName, new Date(year, month, i)));
            hol = new Holiday(holidayName, new Date(year, month, i));
        }
      }
    }
    holiday.push(hol);
}
export function setHol(currentMonthInfo: CurrentMonthInfo) {
    holiday = [];
    getHoliday(currentMonthInfo.year);
    return holiday;
}