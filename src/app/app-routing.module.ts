import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
// import { WelcomeResolver } from './welcome/welcome-resolver';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'welcome', loadChildren: './welcome/welcome.module#WelcomePageModule'},
  { path: 'calendar', loadChildren: './common/pages/calendar/calendar.module#CalendarPageModule' },
  { path: 'event', loadChildren: './common/pages/event/event.module#EventPageModule' },
  { path: 'purchase', loadChildren: './common/pages/purchase/purchase.module#PurchasePageModule' },
  { path: 'alert', loadChildren: './common/pages/alert/alert.module#AlertPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
