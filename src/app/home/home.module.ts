import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CalendarPage } from '../common/pages/calendar/calendar.page';
import { PipesModule } from '../common/pipe/pipes.module';

import { HomePage } from './home.page';
import { SegmentComponent } from '../common/pages/segment/segment.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [HomePage, CalendarPage, SegmentComponent]
})
export class HomePageModule {}
