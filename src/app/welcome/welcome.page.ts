import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../common/services/data.service';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { Storage } from '@ionic/storage';
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';
import { PopoverController } from '@ionic/angular';
import { PurchasePage } from '../common/pages/purchase/purchase.page';
import { AlertPage } from '../common/pages/alert/alert.page';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
  providers: [EmailComposer, InAppPurchase, InAppBrowser]
})
export class WelcomePage implements OnInit {
  selectEvent: string;
  color: any[];
  selectColor: any[] = [];

  constructor(private router: Router,
              private data: DataService,
              private emailComposer: EmailComposer,
              private storage: Storage,
              private iap: InAppPurchase,
              private popCtrl: PopoverController,
              private iab: InAppBrowser) { }

  ngOnInit() {
    this.selectingColor();
  }
  ionViewWillEnter() {
    this.data.getDistrictColor();
  }
  ionViewDidEnter() {
    this.color = this.data.color;
    this.selectEvent = this.data.selectEvent;

  }
  selectingColor() {
    this.selectColor.push(['blue', 'green', '#876418', 'red', 'black', '#FFEC00']);
    this.selectColor.push(['black', 'red', 'blue', 'gold', 'green', '#8F03FF']);
    this.selectColor.push(['black', 'red', 'blue', '#876418', 'green', '#8F03FF']);
    this.selectColor.push(['#FFEC00', '#876418', 'orange', 'red', 'green', 'blue']);
  }
  gotoHome() {
    const data = {
      color: this.color,
      selectEvent: this.selectEvent
    };
    this.data.setWelcomeStatus();
    this.storage.set('theme', data);
    this.router.navigateByUrl('/home');
  }
  async selectAlert() {
    const popover = await this.popCtrl.create({
      component: AlertPage,
      componentProps: {
        color: this.selectColor
      },
      cssClass: 'my-custom-select-css'
    });
    await popover.present();
    popover.onDidDismiss().then((data: any) => {
      if (data.data) {
        this.color = data.data;
        this.data.setDistrictColor(this.color);
      }
    });
  }
  async purchase() {
    const popover = await this.popCtrl.create({
      component: PurchasePage,
      componentProps: {
      },
      cssClass: 'my-custom-purchase-css'
    });
    await popover.present();
    }
  contact() {
    this.emailComposer.isAvailable().then((available: boolean) => {
      if (available) {

      }
    });

    const email = {
      to: 'PostaCalendar@gmail.com',
      subject: 'Postal Calendar',
      body: 'Let me Know what you think!',
      isHtml: true
    };
    this.emailComposer.open(email);
  }
  policy() {
    const browser = this.iab.create('https://sites.google.com/view/rjspostalcalendarprivacypolicy/home', '_system', 'usewkwebview=yes' );

    browser.show();
  }

}
