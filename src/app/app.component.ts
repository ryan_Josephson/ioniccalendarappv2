import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router} from '@angular/router';
import { Storage } from '@ionic/storage';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private storage: Storage,
    private mobileAcessibility: MobileAccessibility,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if(this.mobileAcessibility) {
        this.mobileAcessibility.usePreferredTextZoom(false);
      }
      this.storage.get('wasWelcomeOnce').then((event) => {
        if (event === null) {
          this.storage.set('wasWelcomeOnce', false);
          this.router.navigateByUrl('/welcome');
        } else {
          this.router.navigateByUrl('/home');
        }
      });

      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
