import { NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { RouteReuseStrategy, PreloadAllModules} from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { EventPageModule } from './common/pages/event/event.module';
import { PurchasePageModule } from './common/pages/purchase/purchase.module';
import { AlertPageModule } from './common/pages/alert/alert.module';
import {IonicGestureConfig} from './common/utils/hammerconfig';

@NgModule({
  declarations: [AppComponent, ],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, IonicStorageModule.forRoot(), 
    EventPageModule, PurchasePageModule, AlertPageModule],
  providers: [
    PreloadAllModules,
    StatusBar,
    SplashScreen,
    MobileAccessibility,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: IonicGestureConfig
  }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
